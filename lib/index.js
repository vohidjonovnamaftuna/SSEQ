"use strict";

var _lodash = _interopRequireDefault(require("lodash"));

require("./style.css");

var _image = _interopRequireDefault(require("./image.jpg"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function component() {
  var element = document.createElement('div'); // Lodash, now imported by this script

  element.innerHTML = _lodash["default"].join(['Hello', 'webpack'], ' ');
  element.classList.add('hello'); // Add the image to our existing div.

  var myIcon = new Image();
  myIcon.src = _image["default"];
  element.appendChild(myIcon);
  return element;
}

document.body.appendChild(component());